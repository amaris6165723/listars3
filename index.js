
const { v4: uuidv4 } = require('uuid');
const express = require('express');
const serverless = require('serverless-http')
const AWS = require('aws-sdk');
require('dotenv').config()


// Configuración de AWS y DynamoDB
AWS.config.update({
    region: process.env.REGION
});
const s3 = new AWS.S3();

const app = express();
const port = process.env.APP_PORT;
console.log("port app", port)
app.post('/listars3', async (req, res) => {
    const params = {
        Bucket: "listars3-bucket"
    };
    await s3.listObjects(params).promise().then((data) => {
        res.send(data);
    }, error => {
        res.send(error);
    }
    )
});


if (process.env.ENVIRONMENT == 'production') {
    module.exports.handler = serverless(app);
} else if (process.env.ENVIRONMENT == 'jest') {
    module.exports = app;
}
else {
    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}`)
    })
}